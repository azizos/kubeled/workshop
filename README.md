
## Kubernetes Getting Started

This repository is being used to provide a getting started project for Kubernetes.
You can fork and setup your own playground.

If topics covered during presentations require a related demo, this repository will be updated. 

###  How to use this project

- Please make sure your environment is ready: install both [gcloud](https://cloud.google.com/sdk/install) and [kubectl](https://kubernetes.io/docs/tasks/tools/install-kubectl/).
- Clone the project and change your working directory to the cloned project
- Create a secret to connect to the Gitlab registry to pull an image from it.
- Create your first deployment: `kubectl create -f deployment.yaml`. It's deploys the official image for Nginx.
- Create your first service: `kubectl create -f service.yaml`.
- Check the existense of all created resources:
	- `kubectl get pods`
	- `kubectl get deployments`
	- `kubectl get services`
- Forward the exposed port to your localhost: 
	- `kubectl port-forward service/nginx-service 9001:80`
- Go on your browser and open _localhost:9001_ to see if your deployment is running.

### Workshop instructions
https://1aziz.dev/kubeled2