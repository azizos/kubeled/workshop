FROM python:3.7-alpine

WORKDIR /opt
ADD . .
RUN pip install gunicorn==19.9.0 -r requirements.txt

EXPOSE 3000

ENTRYPOINT ["gunicorn", "-b", "0.0.0.0:3000", "app:app"]